import rest_framework_filters as filters

# See AssetsFilter -> location -> filter_location
# Needed only if we want to follow the proper way to get asset location

# from django.conf import settings
# from rest_framework.exceptions import ParseError
# from django.db.models.expressions import RawSQL

# --------------------------

# Needed only if we want to follow the terrible way to get asset location
# sqlite3 does not support DISTINCT ON to get distinct values of a particular field/fields.

from .serializers import AssetsSerializer

# --------------------------

from .models import (
	Assets,
	Employee,
	AssetLocations,
	AssetLocationRel,
	STEPLocations,
	Categories,
	Currency,
	Donors,
	Carriers,
	ScrapState,
	Items,
)

class AssetsFilter(filters.FilterSet):
	location = filters.Filter(method = 'filter_location')
	class Meta:
		model = Assets
		fields = {
			'id': ['exact'],
			'purchase_date': ['lte', 'gte'],
			'make': ['icontains'],
			'model': ['icontains'],
			'serial_number': ['icontains'],
			'purchase_invoice': ['icontains'],
			'transaction_number': ['icontains'],
			'price': ['lte', 'gte'],
			'currency': ['exact'],
			'vendor': ['icontains'],
			'category': ['exact'],
			'iemi': ['icontains'],
			'iccid': ['icontains'],
			'donor': ['exact'],
			'carrier': ['exact'],
		}

	def filter_location(self, queryset, name, val):
		# The terrible way of doing it, because of sqlite
		location_rel = AssetLocationRel.objects.filter(**{name: val}).values('asset')
		assets = []
		for relation in location_rel:
			asset = AssetsSerializer(Assets.objects.get(id = relation['asset'])).data
			loc_name = asset['location']
			loc = AssetLocations.objects.get(name = loc_name)

			if loc.id == int(val):
				assets.append(asset['id'])
		return queryset.filter(id__in = assets)

		# if we want to avoid the terrible way, throw an error
		if 'sqlite3' in settings.DATABASES['default']['ENGINE'].split('.'):
			raise ParseError({'location': 'The sqlite3 DBMS cannot return latest locations!'})
			return queryset

		# The proper way to do it if we had postgresql
		return queryset.filter(
			id__in = RawSQL('SELECT asset_id \
				FROM( \
					SELECT DISTINCT ON (asset_id) asset_id, location_id \
					FROM assets_assetlocationrel \
					GROUP BY asset_id, date, location_id \
					ORDER BY asset_id, date DESC \
				) AS TBL \
				WHERE TBL.location_id = %s',
				[val]
			)
		)

class EmployeeFilter(filters.FilterSet):
	class Meta:
		model = Employee
		fields = {
			'employee_id': ['startswith'],
			'first_name': ['icontains'],
			'second_name': ['icontains'],
			'location': ['exact']
		}

class AssetLocationFilter(filters.FilterSet):
	class Meta:
		model = AssetLocations
		fields = {
			'name': ['icontains']
		}

class ItemsFilter(filters.FilterSet):
	class Meta:
		models = Items
		fields = {
			'name': ['icontains']
		}

class STEPLocationFilter(filters.FilterSet):
	class Meta:
		model = STEPLocations
		fields = {
			'name': ['icontains']
		}

class CategoryFilter(filters.FilterSet):
	class Meta:
		model = Categories
		fields = {
			'name': ['icontains']
		}

class CurrencyFilter(filters.FilterSet):
	class Meta:
		model = Currency
		fields = {
			'name': ['icontains']
		}

class DonorsFilter(filters.FilterSet):
	class Meta:
		model = Donors
		fields = {
			'name': ['icontains']
		}

class CarriersFilter(filters.FilterSet):
	class Meta:
		model = Carriers
		fields = {
			'name': ['icontains']
		}

class ScrapStateFilter(filters.FilterSet):
	class Meta:
		model = ScrapState
		fields = {
			'name': ['icontains']
		}

class AssetLocationRelFilter(filters.FilterSet):
	class Meta:
		model = AssetLocationRel
		fields = {
			'asset': ['exact']
		}
