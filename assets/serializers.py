from rest_framework.serializers import ModelSerializer
from rest_framework.relations import RelatedField
from rest_framework import serializers

from django.contrib.auth.models import User

from decimal import Decimal

from .models import (
	Assets,
	Employee,
	STEPLocations,
	AssetLocations,
	AssetUser,
	Categories,
	Currency,
	Donors,
	ScrapState,
	Scrap,
	AssetDocuments,
	AssetLocationRel,
	Carriers,
	Items,
)

class AssetDocumentsSerializer(ModelSerializer):
	class Meta:
		model = AssetDocuments
		fields = '__all__'

class EmployeeSerializer(ModelSerializer):
	location = serializers.SlugRelatedField(slug_field = 'name', queryset = STEPLocations.objects.all())
	class Meta:
		model = Employee
		fields = '__all__'

class STEPLocationsSerializer(ModelSerializer):
	class Meta:
		model = STEPLocations
		fields = '__all__'

class AssetLocationsSerializer(ModelSerializer):
	class Meta:
		model = AssetLocations
		fields = '__all__'

class CategorySerializer(ModelSerializer):
	class Meta:
		model = Categories
		fields = '__all__'

class AssetUserSerializer(ModelSerializer):
	class Meta:
		model = AssetUser
		fields = '__all__'

class ROAssetUserSerializer(ModelSerializer):
	class Meta:
		model = AssetUser
		depth = 1
		fields = '__all__'

class AssetHistoryDateSerializer(ModelSerializer):
	class Meta:
		model = AssetUser
		depth = 1
		fields = '__all__'

class CurrencySerializer(ModelSerializer):
	class Meta:
		model = Currency
		fields = '__all__'

class DonorsSerializer(ModelSerializer):
	class Meta:
		model = Donors
		fields = '__all__'

class CarriersSerializer(ModelSerializer):
	class Meta:
		model = Carriers
		fields = '__all__'

class ScrapStateSerializer(ModelSerializer):
	class Meta:
		model = ScrapState
		fields = '__all__'

class EmployeeNameField(serializers.RelatedField):
	def to_representation(self, instance):
		return '{2} {0} {1}'.format(instance.first_name, instance.second_name, instance.employee_id)
	def to_internal_value(self, data):
		name = data.split(' ')
		return self.get_queryset().get(first_name = name[0], second_name = name[1], employee_id = name[2])

class ScrapSerializer(ModelSerializer):
	authorized_by = EmployeeNameField(queryset = Employee.objects.all())
	scrap_requested_by = EmployeeNameField(queryset = Employee.objects.all())
	state = serializers.SlugRelatedField(slug_field = 'name', queryset = ScrapState.objects.all())
	class Meta:
		model = Scrap
		fields = '__all__'

class AssetPriceFieldSerializer(serializers.DecimalField):
	def to_representation(self, instance):
		x = Decimal(instance)
		return f'{x:,}'

	def to_internal_value(self, data):
		if ',' in data:
			x = data
			x = x.replace(',', '')
			return Decimal(x)
		return data

class ItemsSerializer(ModelSerializer):
	class Meta:
		model = Items
		fields = ('name',)

class AssetsSerializer(ModelSerializer):
	purchase_date = serializers.DateField(input_formats = ['%d-%m-%Y', '%Y-%m-%d'])
	in_posession_of = serializers.SerializerMethodField()
	location = serializers.SerializerMethodField()
	category = serializers.SlugRelatedField(slug_field = 'name', required = False, queryset = Categories.objects.all())
	currency = serializers.SlugRelatedField(slug_field = 'name', required = False, queryset = Currency.objects.all())
	accompanied_items = serializers.SlugRelatedField(slug_field = 'name', allow_empty = True, required = False, many = True, queryset = Items.objects.all())
	donor = serializers.SlugRelatedField(slug_field = 'name', required = False, queryset = Donors.objects.all())
	documents = AssetDocumentsSerializer(source = 'assetdocuments_set', required = False, many = True)
	price = AssetPriceFieldSerializer(max_digits = 13, required = False, decimal_places = 2, coerce_to_string = True)
	registered_name = EmployeeNameField(required = False, queryset = Employee.objects.all())

	class Meta:
		model = Assets
		fields = '__all__'

	@staticmethod
	def setup_eager_loading(queryset):
		queryset = queryset.prefetch_related(
			'assetuser_set',
			'assetdocuments_set',
			'assetlocationrel_set',
		)
		return queryset

	def get_in_posession_of(self, obj):
		users = obj.assetuser_set.all()

		if len(users) > 0:
			max_date = users[0]
			for i in range(1, len(users)):
				if users[i].date_given_back > max_date.date_given_back:
					max_date = users[i]
			return max_date.employee.first_name + ' ' + max_date.employee.second_name

		return None

	def get_location(self, obj):
		location_rel = obj.assetlocationrel_set.all()

		if len(location_rel) > 0:
			max_date = location_rel[0]
			for i in range(1, len(location_rel)):
				if location_rel[i].date > max_date.date:
					max_date = location_rel[i]
			return max_date.location.name

		return None

class AssetNameField(serializers.RelatedField):
	def to_representation(self, instance):
		return '{0} | {1} | {2}'.format(instance.make, instance.model, instance.id)
	def to_internal_value(self, data):
		if data.isnumeric():
			return self.get_queryset().get(id = data)
		name = data.split(' | ')
		return self.get_queryset().get(make = name[0], model = name[1], id = name[2])

class AssetLocationRelSerializer(ModelSerializer):
	asset = AssetNameField(queryset = Assets.objects.all())
	location = serializers.SlugRelatedField(slug_field = 'name', queryset = AssetLocations.objects.all())
	class Meta:
		model = AssetLocationRel
		fields = '__all__'

class UserSerializer(ModelSerializer):
	class Meta:
		model = User
		exclude = ('password', 'last_login', 'date_joined')

class AssetFilterSerializer(serializers.Serializer):
	id = serializers.ListField(child = serializers.IntegerField())
	purchase_date = serializers.ListField(child = serializers.DateField(input_formats = ['%d-%m-%Y', '%Y-%m-%d']))
	make = serializers.ListField(child = serializers.CharField(max_length = 64))
	model = serializers.ListField(child = serializers.CharField(max_length = 64))
	serial_number = serializers.ListField(child = serializers.CharField(max_length = 64))
	purchase_invoice = serializers.ListField(child = serializers.IntegerField())
	price = serializers.ListField(child = serializers.DecimalField(max_digits = 10, decimal_places = 2))
	currency = CurrencySerializer(many = True, read_only = True)
	donor = DonorsSerializer(many = True, read_only = True)
	carrier = CarriersSerializer(many = True, read_only = True)
	vendor = serializers.ListField(child = serializers.CharField(max_length = 128))
	location = AssetLocationRelSerializer(many = True, read_only = True)
	category = CategorySerializer(many = True, read_only = True)
	scrapped = serializers.ListField(child = serializers.BooleanField(default = False))
	iemi = serializers.ListField(child = serializers.CharField(max_length = 16))
	iccid = serializers.ListField(child = serializers.CharField(max_length = 17))
	type = serializers.ListField(child = serializers.CharField(max_length = 256))
	including = serializers.ListField(child = serializers.CharField(max_length = 256))
	active_since = serializers.ListField(child = serializers.DateField(input_formats = ['%d-%m-%Y', '%Y-%m-%d']))

class EmployeeFilterSerializer(serializers.Serializer):
	id = serializers.ListField(child = serializers.IntegerField())
	employee_id = serializers.ListField(child = serializers.IntegerField())
	first_name = serializers.ListField(child = serializers.CharField(max_length = 32))
	second_name = serializers.ListField(child = serializers.CharField(max_length = 32))
	location = STEPLocationsSerializer(many = True, read_only = True)

class AssetLocationFilterSerializer(serializers.Serializer):
	id = serializers.ListField(child = serializers.IntegerField())
	name = serializers.ListField(child = serializers.CharField(max_length = 256))

class STEPLocationFilterSerializer(serializers.Serializer):
	id = serializers.ListField(child = serializers.IntegerField())
	name = serializers.ListField(child = serializers.CharField(max_length = 256))

class CategoryFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 256))

class CurrencyFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 32))

class DonorsFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 256))

class CarriersFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 256))

class ScrapStateFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 32))

class AssetLocationRelFilterSerializer(serializers.Serializer):
	asset = AssetsSerializer(many = True, read_only = True)

class ItemsFilterSerializer(serializers.Serializer):
	name = serializers.ListField(child = serializers.CharField(max_length = 256))
