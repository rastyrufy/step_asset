from django.db import models

def asset_primary_image(instance, filename):
	return "assets/{0}_{1}/{2}".format(instance.make, instance.model, filename)

def asset_documents(instance, filename):
	return "assets/{0}_{1}/documents/{2}".format(instance.asset.make, instance.asset.model, filename)

class STEPLocations(models.Model):
	name = models.CharField(max_length = 256, unique = True)

	def __str__(self):
		return self.name

class AssetLocations(models.Model):
	name = models.CharField(max_length = 256, unique = True)

	def __str__(self):
		return self.name

class Categories(models.Model):
	name = models.CharField(max_length = 256)

	def __str__(self):
		return self.name

class Employee(models.Model):
	employee_id = models.IntegerField(unique = True)
	first_name = models.CharField(max_length = 64)
	second_name = models.CharField(max_length = 64)
	location = models.ForeignKey(STEPLocations, on_delete = models.PROTECT)

	def __str__(self):
		return "{0} {1}".format(self.first_name, self.second_name)

class Currency(models.Model):
	name = models.CharField(max_length = 32)

class Donors(models.Model):
	name = models.CharField(max_length = 256)

class Carriers(models.Model):
	name = models.CharField(max_length = 128)

class Items(models.Model):
	name = models.CharField(max_length = 256, unique = True)

	def __str__(self):
		return self.name

class Assets(models.Model):
	make = models.CharField(max_length = 64, null = True, blank = True)
	model = models.CharField(max_length = 64, null = True, blank = True)
	serial_number = models.CharField(max_length = 64, null = True, blank = True)
	purchase_invoice = models.IntegerField(unique = True, null = True)
	transaction_number = models.IntegerField(unique = True)
	price = models.DecimalField(max_digits = 13, decimal_places = 2, null = True, blank = True)
	currency = models.ForeignKey(Currency, on_delete = models.PROTECT, null = True)
	donor = models.ForeignKey(Donors, on_delete = models.PROTECT, null = True)
	description = models.TextField()
	picture = models.ImageField(upload_to = asset_primary_image, null = True)
	comment = models.TextField()
	purchase_date = models.DateField()
	vendor = models.CharField(max_length = 128, null = True, blank = True)
	location = models.ManyToManyField(
		AssetLocations,
		through = 'AssetLocationRel',
		through_fields = ('asset', 'location')
	)
	category = models.ForeignKey(Categories, on_delete = models.PROTECT, null = True)
	accompanied_items = models.ManyToManyField(Items)
	other_accompanied_items = models.TextField(null = True, blank = True)

	iemi = models.CharField(max_length = 16, null = True, blank = True)
	iccid = models.CharField(max_length = 17, null = True, blank = True)
	type = models.CharField(max_length = 256, null = True, blank = True)
	including = models.CharField(max_length = 256, null = True, blank = True)
	active_since = models.DateField(null = True, blank = True)
	carrier = models.ForeignKey(Carriers, on_delete = models.SET_NULL, null = True)

	frame_number = models.IntegerField(null = True, blank = True)
	plate_number = models.IntegerField(null = True, blank = True)
	manufacturing_year = models.DateField(null = True, blank = True)
	registration_number = models.IntegerField(null = True, blank = True)
	registered_name = models.ForeignKey(Employee, on_delete = models.SET_NULL, null = True)

	scrapped = models.BooleanField(default = False)

	def __str__(self):
		return "{0} | {0}".format(self.make, self.model)

class AssetLocationRel(models.Model):
	asset = models.ForeignKey(Assets, on_delete = models.PROTECT)
	location = models.ForeignKey(AssetLocations, on_delete = models.PROTECT)
	date = models.DateField()

class AssetDocuments(models.Model):
	asset = models.ForeignKey(Assets, on_delete = models.CASCADE)
	name = models.FileField(upload_to = asset_documents)

class AssetUser(models.Model):
	asset = models.ForeignKey(Assets, on_delete = models.CASCADE)
	employee = models.ForeignKey(Employee, on_delete = models.CASCADE)
	date_recieved = models.DateField()
	date_given_back = models.DateField(null = True)
	notes = models.TextField(null = True, blank = True)

class ScrapState(models.Model):
	name = models.CharField(max_length = 32, unique = True)

class Scrap(models.Model):
	asset = models.OneToOneField(Assets, on_delete = models.CASCADE)
	scrap_requested_by = models.ForeignKey(Employee, on_delete = models.PROTECT)
	authorized_by = models.ForeignKey(Employee, on_delete = models.PROTECT, related_name = 'authorized_by')
	state = models.ForeignKey(ScrapState, on_delete = models.SET_NULL, null = True)
	date = models.DateField()
	reasons = models.TextField(null = True, blank = True)
