from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView

from rest_framework.response import Response
from rest_framework.decorators import api_view, action

from rest_framework import serializers

from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate, login

from rest_framework_filters.backends import ComplexFilterBackend
from django.db.models import Min, Max

from .paginations import (
	AssetsPagination,
	EmployeePagination,
	HistoryPagination,
	AssetLocationPagination,
	STEPLocationPagination,
	CategoryPagination,
	CurrencyPagination,
	DonorsPagination,
	CarriersPagination,
	ItemsPagination,
)

from .models import (
	Assets,
	Employee,
	STEPLocations,
	AssetLocations,
	AssetUser,
	Categories,
	Currency,
	Donors,
	Carriers,
	ScrapState,
	Scrap,
	AssetDocuments,
	AssetLocationRel,
	Items,
)

from .serializers import (
	AssetsSerializer,
	EmployeeSerializer,
	STEPLocationsSerializer,
	AssetLocationsSerializer,
	AssetUserSerializer,
	ROAssetUserSerializer,
	CategorySerializer,
	CurrencySerializer,
	DonorsSerializer,
	CarriersSerializer,
	ScrapStateSerializer,
	ScrapSerializer,
	AssetDocumentsSerializer,
	AssetLocationRelSerializer,
	ItemsSerializer,

	AssetFilterSerializer,
	EmployeeFilterSerializer,
	AssetLocationFilterSerializer,
	STEPLocationFilterSerializer,
	CategoryFilterSerializer,
	CurrencyFilterSerializer,
	DonorsFilterSerializer,
	CarriersFilterSerializer,
	ScrapStateFilterSerializer,
	AssetLocationRelFilterSerializer,
	ItemsFilterSerializer,

	UserSerializer,
)

from .filters import (
	AssetsFilter,
	EmployeeFilter,
	AssetLocationFilter,
	STEPLocationFilter,
	CategoryFilter,
	CurrencyFilter,
	DonorsFilter,
	CarriersFilter,
	ScrapStateFilter,
	AssetLocationRelFilter,
	ItemsFilter,
)

class ItemsAPI(ModelViewSet):
	queryset = Items.objects.all()
	serializer_class = ItemsSerializer
	filter_class = ItemsFilter
	filterset_serializer = ItemsFilterSerializer
	pagination_class = ItemsPagination

class AssetsAPI(ModelViewSet):
	# queryset = Assets.objects.filter(scrapped = False).order_by('id').reverse()
	serializer_class = AssetsSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = AssetsFilter
	filterset_serializer = AssetFilterSerializer
	pagination_class = AssetsPagination

	def get_queryset(self):
		queryset = Assets.objects.filter(scrapped = False).order_by('id').reverse()
		if self.request.method in (['GET']):
			queryset = self.get_serializer_class().setup_eager_loading(queryset)
		return queryset

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data = request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		
		asset = Assets.objects.get(id = serializer.data['id'])

		document_keys = list(filter(lambda key: 'documents_' in key, request.data))

		for document in document_keys:
			asset_document = AssetDocuments(asset = asset, name = request.data[document])
			asset_document.save()

		return Response(serializer.data)


	@action(methods = ['GET'], detail = False)
	def filters(self, request):
		filter_info = {
			'id': {
				'title': 'ID',
				'view_type': 'input',
				'query_types': [''],
				'data': self.get_queryset().aggregate(Min('id'), Max('id'))
			},
			'purchase_date': {
				'title': 'Date Purchased',
				'view_type': 'date_range',
				'query_types': ['__gte', '__lte']
			},
			'make':	{
				'title': 'Make',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'model':		  {
				'title': 'Model',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'serial_number':  {
				'title': 'Serial Number',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'purchase_invoice': {
				'title': 'Purchase Invoice',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'transaction_number': {
				'title': 'Transaction Number',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'vendor': {
				'title': 'Vendor',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'location':		  {
				'title': 'Location',
				'view_type': 'select',
				'query_types': [''],
				'table': 'asset_locations',
				'field': 'name',
				'data': AssetLocations.objects.all()[:5].values(),
			},
			'category': {
				'title': 'Category',
				'view_type': 'select',
				'query_types': [''],
				'table': 'categorys',
				'field': 'name',
				'data': Categories.objects.all()[:5].values(),
			},
			'currency': {
				'title': 'Currency',
				'view_type': 'select',
				'query_types': [''],
				'table': 'currencys',
				'field': 'name',
				'data': Currency.objects.all()[:5].values(),
			},
			'donor': {
				'title': 'Donor',
				'view_type': 'select',
				'query_types': [''],
				'table': 'donors',
				'field': 'name',
				'data': Donors.objects.all()[:5].values(),
			},
			'carrier': {
				'title': 'Carrier',
				'view_type': 'select',
				'query_types': [''],
				'table': 'carriers',
				'field': 'name',
				'data': Carriers.objects.all()[:5].values(),
			},
			'iemi': {
				'title': 'IEMI',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'iccid': {
				'title': 'ICCID',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'price': {
				'title': 'Price',
				'view_type': 'slider',
				'query_types': ['__gte', '__lte'],
				'data': self.get_queryset().aggregate(Min('price'), Max('price'))
			},
			'accompanied_items': {
				'title': 'Accompanied Items',
				'view_type': 'multiple_select',
				'query_type': [''],
				'data': Items.objects.all().values()
			},
		}
		return Response(filter_info)


	@action(methods = ['GET'], detail = False)
	def asset_list(self, request):
		if 'name' in request.GET:
			data = self.queryset.filter(make__icontains = request.GET.get('name')) | self.queryset.filter(model__icontains = request.GET.get('name'))
			serial = AssetsSerializer(data, many = True).data
		return Response(serial)

class CurrencyAPI(ModelViewSet):
	queryset = Currency.objects.all()
	serializer_class = CurrencySerializer
	permission_classes = (IsAuthenticated,)
	filter_class = CurrencyFilter
	filterset_serializer = CurrencyFilterSerializer
	pagination_class = CurrencyPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)

class DonorsAPI(ModelViewSet):
	queryset = Donors.objects.all()
	serializer_class = DonorsSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = DonorsFilter
	filterset_serializer = DonorsFilterSerializer
	pagination_class = DonorsPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)

class CarriersAPI(ModelViewSet):
	queryset = Carriers.objects.all()
	serializer_class = CarriersSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = CarriersFilter
	filterset_serializer = CarriersFilterSerializer
	pagination_class = CarriersPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)

class AssetLocationRelAPI(ModelViewSet):
	queryset = AssetLocationRel.objects.all().order_by('-date')
	serializer_class = AssetLocationRelSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = AssetLocationRelFilter
	filterset_serializer = AssetLocationRelFilterSerializer

class ScrappedAssetsAPI(ModelViewSet):
	queryset = Assets.objects.filter(scrapped = True).order_by('id').reverse()
	serializer_class = AssetsSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = AssetsFilter
	filterset_serializer = AssetFilterSerializer
	pagination_class = AssetsPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):
		filter_info = {
			'id': {
				'title': 'ID',
				'view_type': 'input',
				'query_types': [''],
				'data': self.get_queryset().aggregate(Min('id'), Max('id'))
			},
			'purchase_date': {
				'title': 'Date Purchased',
				'view_type': 'date_range',
				'query_types': ['__gte', '__lte']
			},
			'make':	{
				'title': 'Make',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'model':		  {
				'title': 'Model',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'serial_number':  {
				'title': 'Serial Number',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'purchase_invoice': {
				'title': 'Purchase Invoice',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'transaction_number': {
				'title': 'Transaction Number',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'vendor': {
				'title': 'Vendor',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'location':		  {
				'title': 'Location',
				'view_type': 'select',
				'query_types': [''],
				'table': 'asset_locations',
				'field': 'name',
				'data': AssetLocations.objects.all()[:5].values(),
			},
			'category': {
				'title': 'Category',
				'view_type': 'select',
				'query_types': [''],
				'table': 'categorys',
				'field': 'name',
				'data': Categories.objects.all()[:5].values(),
			},
			'currency': {
				'title': 'Currency',
				'view_type': 'select',
				'query_types': [''],
				'table': 'currencys',
				'field': 'name',
				'data': Currency.objects.all()[:5].values(),
			},
			'donor': {
				'title': 'Donor',
				'view_type': 'select',
				'query_types': [''],
				'table': 'donors',
				'field': 'name',
				'data': Donors.objects.all()[:5].values(),
			},
			'carrier': {
				'title': 'Carrier',
				'view_type': 'select',
				'query_types': [''],
				'table': 'carriers',
				'field': 'name',
				'data': Carriers.objects.all()[:5].values(),
			},
			'iemi': {
				'title': 'IEMI',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'iccid': {
				'title': 'ICCID',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'price': {
				'title': 'Price',
				'view_type': 'slider',
				'query_types': ['__gte', '__lte'],
				'data': self.get_queryset().aggregate(Min('price'), Max('price'))
			},
			'accompanied_items': {
				'title': 'Accompanied Items',
				'view_type': 'multiple_select',
				'query_type': [''],
				'data': Items.objects.all().values()
			},
		}
		return Response(filter_info)


	@action(methods = ['GET'], detail = False)
	def asset_list(self, request):
		if 'name' in request.GET:
			data = self.queryset.filter(make__icontains = request.GET.get('name')) | self.queryset.filter(model__icontains = request.GET.get('name'))
			serial = AssetsSerializer(data, many = True).data
		return Response(serial)

class ScrapStateAPI(ModelViewSet):
	queryset = ScrapState.objects.all()
	serializer_class = ScrapStateSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = ScrapStateFilter
	filterset_serializer = ScrapStateFilterSerializer

class ScrapAPI(ModelViewSet):
	lookup_field = 'asset'
	queryset = Scrap.objects.all()
	serializer_class = ScrapSerializer
	permission_classes = (IsAuthenticated,)

class EmployeeAPI(ModelViewSet):
	queryset = Employee.objects.all().order_by('id').reverse()
	serializer_class = EmployeeSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = EmployeeFilter
	filterset_serializer = EmployeeFilterSerializer
	pagination_class = EmployeePagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'employee_id': {
				'title': 'Employee ID',
				'view_type': 'input',
				'query_types': ['__startswith']
			},
			'first_name': {
				'title': 'First Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'second_name': {
				'title': 'Second Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			},
			'location':		  {
				'title': 'Location',
				'view_type': 'select',
				'query_types': [''],
				'table': 'step_locations',
				'field': 'name',
				'data': STEPLocations.objects.all()[:5].values(),
			}
		}

		return Response(filter_info)

	@action(methods = ['GET'], detail = False)
	def employee_list(self, request):
		serial = {}
		if 'name' in request.GET:
			data = self.queryset.filter(first_name__icontains = request.GET.get('name')) | self.queryset.filter(second_name__icontains = request.GET.get('name'))
			serial = EmployeeSerializer(data, many = True).data
		return Response(serial)


class AssetLocationsAPI(ModelViewSet):
	queryset = AssetLocations.objects.all().order_by('id').reverse()
	serializer_class = AssetLocationsSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = AssetLocationFilter
	filterset_serializer = AssetLocationFilterSerializer
	pagination_class = AssetLocationPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)


class STEPLocationsAPI(ModelViewSet):
	queryset = STEPLocations.objects.all().order_by('id').reverse()
	serializer_class = STEPLocationsSerializer
	permission_classes = (IsAuthenticated,)
	filter_class = STEPLocationFilter
	filterset_serializer = STEPLocationFilterSerializer
	pagination_class = STEPLocationPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)


class CategoriesAPI(ModelViewSet):
	queryset = Categories.objects.all().order_by('id').reverse()
	serializer_class = CategorySerializer
	permission_classes = (IsAuthenticated,)
	filter_class = CategoryFilter
	filterset_serializer = CategoryFilterSerializer
	pagination_class = CategoryPagination

	@action(methods = ['GET'], detail = False)
	def filters(self, request):

		filter_info = {
			'name': {
				'title': 'Name',
				'view_type': 'input',
				'query_types': ['__icontains']
			}
		}

		return Response(filter_info)

class AssetUserAPI(ModelViewSet):
	queryset = AssetUser.objects.all().order_by('id').reverse()
	serializer_class = AssetUserSerializer
	permission_classes = (IsAuthenticated,)

class HistoryAPI(ListAPIView):
	serializer_class = ROAssetUserSerializer
	permission_classes = (IsAuthenticated,)
	pagination_class = HistoryPagination

	def get_queryset(self):
		employee = self.request.query_params.get('employee')
		asset = self.request.query_params.get('asset') or self.request.query_params.get('scrapped_asset')
		if employee is not None:
			return AssetUser.objects.filter(employee = employee).order_by('-date_recieved')
		elif asset is not None:
			return AssetUser.objects.filter(asset = asset).order_by('-date_recieved')

class AssetDocumentsAPI(ModelViewSet):
	queryset = AssetDocuments.objects.all()
	serializer_class = AssetDocumentsSerializer
	permission_classes = (IsAuthenticated,)

@api_view(['POST'])
def user_login(request):
	user = request.user
	if not user.is_authenticated and 'check' not in request.data:
		user = authenticate(request = request, username = request.data['username'], password = request.data['password'])
		if user:
			x = login(request._request, user)
	response = {'user': UserSerializer(user).data}
	return Response(response)

@api_view(['GET'])
def user_logout(request):
	request.session.flush()
	return Response({'user': { 'logged_out': True }})