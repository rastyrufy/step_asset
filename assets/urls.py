from rest_framework.routers import SimpleRouter
from django.urls import path

# from django.contrib.auth.views import logout_then_login

from .views import (
	AssetsAPI,
	EmployeeAPI,
	AssetLocationsAPI,
	STEPLocationsAPI,
	AssetUserAPI,
	HistoryAPI,
	ScrappedAssetsAPI,
	CategoriesAPI,
	CurrencyAPI,
	DonorsAPI,
	CarriersAPI,
	ScrapStateAPI,
	ScrapAPI,
	AssetDocumentsAPI,
	AssetLocationRelAPI,
	ItemsAPI,

	user_login,
	user_logout,
)

router = SimpleRouter()

router.register('assets', AssetsAPI, basename = 'assets')
router.register('employees', EmployeeAPI)
router.register('asset_locations', AssetLocationsAPI)
router.register('step_locations', STEPLocationsAPI)
router.register('asset_user', AssetUserAPI)
router.register('scrapped_assets', ScrappedAssetsAPI)
router.register('categorys', CategoriesAPI)
	# Spelled this way because we want the frontend to dynamically switch between "category" and "categorys"!
router.register('donors', DonorsAPI)
router.register('items', ItemsAPI)
router.register('carriers', CarriersAPI)
router.register('currencys', CurrencyAPI)
router.register('scrap_state', ScrapStateAPI)
router.register('scrap', ScrapAPI)
router.register('asset_documents', AssetDocumentsAPI)
router.register('location_history', AssetLocationRelAPI)


urlpatterns = [
	path('history/', HistoryAPI.as_view(), name = 'history_list'),

	path('login/', user_login),
	path('logout/', user_logout)
]

urlpatterns += router.urls