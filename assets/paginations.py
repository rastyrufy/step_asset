from rest_framework.pagination import LimitOffsetPagination

class AssetsPagination(LimitOffsetPagination):
	default_limit = 10

class EmployeePagination(LimitOffsetPagination):
	default_limit = 10

class HistoryPagination(LimitOffsetPagination):
	default_limit = 10

class AssetLocationPagination(LimitOffsetPagination):
	default_limit = 10

class STEPLocationPagination(LimitOffsetPagination):
	default_limit = 10

class CategoryPagination(LimitOffsetPagination):
	default_limit = 10

class CurrencyPagination(LimitOffsetPagination):
	default_limit = 10

class DonorsPagination(LimitOffsetPagination):
	default_limit = 10

class CarriersPagination(LimitOffsetPagination):
	default_limit = 10

class ItemsPagination(LimitOffsetPagination):
	default_limit = 10
